<%-- 
    Document   : index
    Created on : 14-09-2021, 22:05:46
    Author     : gvejar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bienvenida</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    </head>
    <body>
        <h1>Registro Alumno</h1>
        <form action="RegisterController" method="POST">
            
            <div class="form-group">
              <label for="nombre">Nombre completo</label>
              <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingresa nombre">
            </div>
                        
            <div class="form-group">
              <label for="seccion">Sección</label>
              <input type="text" class="form-control" id="seccion" name="seccion" placeholder="Ingresa sección">
            </div>
            
            <div><button type="submit" class="btn btn-default">Enviar</button></div>
            
        </form>
    </body>
</html>
